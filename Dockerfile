FROM node:10-alpine

COPY image/gatsby.conf /etc/nginx/conf.d/default.conf

RUN apk add --no-cache \
      nginx \
      python \
      build-base \
    && npm install --global --no-optional gatsby@1.9 \
    && mkdir -p /www /run/nginx

WORKDIR /www
EXPOSE 8080

COPY ./home-page /www
RUN npm install
RUN gatsby build

CMD ["nginx", "-g", "daemon off;"]
