import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import SEO from '../components/seo'


const IndexPage = () => {
    return (
        <div>
            <Layout>
                <SEO title="Home" />
                <h1>Hello.</h1>
                <h2>I'm Patryk, a backend developer living in beautiful Poznań.</h2>
                <p>Contact: <Link to="/contact">CLIK THERE</Link></p>
            </Layout>
        </div>
    )
}

export default IndexPage