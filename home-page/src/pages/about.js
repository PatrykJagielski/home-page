import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'


const AboutPage = () => {
    return (
        <div>
            <Layout>
                <h1>About Me</h1>
                <p>I currently work full-time on BBBit.</p>
                <p><Link to="/contact">Contact</Link></p>
            </Layout>
        </div>
    )
}

export default AboutPage